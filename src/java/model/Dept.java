package model;

import Annotation.Chemin;
import Utils.ModelView;

public class Dept {
    String nomDept;

    public Dept() {
    }

    public Dept(String nom) {
        this.nomDept = nom;
    }

    public String getNomDept() {
        return nomDept;
    }

    public void setNomDept(String nom) {
        this.nomDept = nom;
    }

    @Chemin(url = "getDept")
    public static ModelView getDept() {
        ModelView mv = new ModelView();
        mv.setTarget("departement.jsp");
        mv.addAttribute("departement", new Dept("Finance"));
        return mv;
    }
    
    @Chemin(url = "newDept")
    public static ModelView newDept() {
        ModelView mv = new ModelView();
        mv.setTarget("DetailsDept.jsp");
        mv.addAttribute("departement", new Dept("Communication"));
        return mv;
    }
}
